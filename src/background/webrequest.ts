import config from '../config/config';
import Url from '../config/Url';

interface RequestDetails {
  frameId: number;
  originUrl: string;
  tabId: number;
  timeStamp: number;
  type: string;
  url: string;
}

export default class WebRequest {
  public static init(): void {
    browser.webRequest.onBeforeRequest.addListener(
      WebRequest.redirect,
      { urls: [Url.jump5chNet] },
      ['blocking'],
    );
  }

  private static redirect(requestDetails: RequestDetails): {} {
    const parsedUrl = new URL(requestDetails.url);
    const param = parsedUrl.search.substr(1);
    if (!config.REGEXP_URL.test(param)) {
      const redirectUrl = `${config.SCHEME_HTTP}${param}`;
      return { redirectUrl };
    }
    return { redirectUrl: param };
  }
}
